package mision.tic.backendProyecto.controlador;

import mision.tic.backendProyecto.modelo.Generos;
import mision.tic.backendProyecto.servicio.GeneroSerivicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class GeneroControlador {
    @Autowired
    GeneroSerivicio generoSerivicio;

    @GetMapping("/obtenerGeneros")
    public List<Generos> obtenerGeneros(){
        return generoSerivicio.obtenerGeneros();
    }

    @GetMapping("/obtenerGeneroId")
    public Optional<Generos> obtenerGeneroId(@RequestParam int id){
        return generoSerivicio.obtenerGeneroId(id);
    }

    @PostMapping("/crearGenero")
    public boolean crearGenero(@RequestBody Generos generos){
        return generoSerivicio.crearGenero(generos);
    }
}
