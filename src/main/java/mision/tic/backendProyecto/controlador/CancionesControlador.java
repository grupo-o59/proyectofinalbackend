package mision.tic.backendProyecto.controlador;

import mision.tic.backendProyecto.modelo.Canciones;
import mision.tic.backendProyecto.modelo.Generos;
import mision.tic.backendProyecto.servicio.CancionesSerivicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class CancionesControlador {
    @Autowired
    CancionesSerivicio cancionesSerivicio;

    @GetMapping("/obtenerCanciones")
    public List<Canciones> obtenerCanciones(){
        return cancionesSerivicio.obtenerCanciones();
    }

    @PostMapping("/crearCancion")
    public boolean crearCancion(@RequestParam int idGenero,@RequestBody Canciones cancion){
        return cancionesSerivicio.crearCancion(idGenero,cancion);
    }
}
