package mision.tic.backendProyecto.servicio;

import mision.tic.backendProyecto.modelo.Generos;
import mision.tic.backendProyecto.repositorio.GenerosRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GeneroSerivicio {
    @Autowired
    GenerosRepositorio generosRepositorio;

    public List<Generos> obtenerGeneros() {
        return generosRepositorio.findAll();
    }

    public Optional<Generos> obtenerGeneroId(int id) {
        return generosRepositorio.findById(id);
    }

    public boolean crearGenero(Generos generos) {
        try {
            generosRepositorio.save(generos);
            return true;
        }catch (Exception e){
            System.err.println(e);
            return false;
        }
    }
}
