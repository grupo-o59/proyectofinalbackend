package mision.tic.backendProyecto.servicio;

import mision.tic.backendProyecto.modelo.Canciones;
import mision.tic.backendProyecto.modelo.Generos;
import mision.tic.backendProyecto.repositorio.CancionesRepositorio;
import mision.tic.backendProyecto.repositorio.GenerosRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CancionesSerivicio {
    @Autowired
    CancionesRepositorio cancionesRepositorio;
    @Autowired
    GenerosRepositorio generosRepositorio;

    public List<Canciones> obtenerCanciones() {
        return cancionesRepositorio.findAll();
    }

    public boolean crearCancion(int idGenero,Canciones cancion) {
        try{
            Generos genero = generosRepositorio.findById(idGenero).get();
            genero.getCanciones().add(cancion);
            generosRepositorio.save(genero);
            return true;
        }catch (Exception e){
            System.err.println(e);
            return false;
        }
    }
}
