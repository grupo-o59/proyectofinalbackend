package mision.tic.backendProyecto.servicio;

import mision.tic.backendProyecto.entradas.UsuarioEntrada;
import mision.tic.backendProyecto.modelo.Usuario;
import mision.tic.backendProyecto.repositorio.UsuarioRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioServicio {
    @Autowired
    UsuarioRepositorio usuarioRepositorio;

    public boolean iniciarsesion(String correo, String contraseña) {
        if(usuarioRepositorio.existsByCorreo(correo)){
            Usuario usuario = usuarioRepositorio.findByCorreo(correo);
            if(usuario.getContraseña().equals(contraseña)){
                return true;
            }
        }
        return false;
    }

    public boolean agregarusuario(UsuarioEntrada usuarioEntrada) {
        try {
            if (usuarioEntrada.getNombre().isEmpty() || usuarioEntrada.getCorreo().isEmpty() || usuarioEntrada.getContraseña().isEmpty() || usuarioEntrada.getContraseñaRep().isEmpty()){
                return false;
            }else {
                if(usuarioEntrada.getContraseña().equals(usuarioEntrada.getContraseñaRep())){
                    Usuario usuario = new Usuario(usuarioEntrada.getNombre(),usuarioEntrada.getCorreo(), usuarioEntrada.getContraseña());
                    usuarioRepositorio.save(usuario);
                    return true;
                }
                return false;
            }
        }catch (Exception e){
            return false;
        }
    }

    public boolean eliminarusuario(int id) {
        try {
            usuarioRepositorio.deleteById(id);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public Usuario consultarusuario(int id) {
        try {
            return usuarioRepositorio.findById(id).get();
        }catch (Exception e){
            return null;
        }
    }


}
