package mision.tic.backendProyecto.entradas;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioEntrada {
    @NotNull
    private String nombre;
    @NotNull
    private String correo;
    @NotNull
    private String contraseña;
    @NotNull
    private String contraseñaRep;
}
