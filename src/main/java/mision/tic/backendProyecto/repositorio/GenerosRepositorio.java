package mision.tic.backendProyecto.repositorio;

import mision.tic.backendProyecto.modelo.Generos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GenerosRepositorio extends JpaRepository<Generos,Integer> {
}
